export const state = () => ({
    email: '',
    password: '',
    username: '',
  })
  
  export const mutations = {
    clear(state) {
      state.email = '';
      state.password = '';
      state.username = '';
    },
    email(state, value) { state.email = value },
    password(state, value) { state.password = value },
    username(state, value) { state.username = value },
  }
  
  export const actions = {
    async send(context) {
      const response = await this.$axios.$post(
        '/register',
        {
          email: context.state.email,
          password: context.state.password,
          username: context.state.username
        }
      );
      const id = JSON.parse(atob(response.accessToken.split('.')[1])).sub
      this.commit("token/jwt", response.accessToken, { root: true });
      this.$router.push('/users/' + id);
    }
  }
  